<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function Register()
    {
        return view('register');
    }

    public function processPostData(Request $request)
    {
        $data = [
            "nama_depan" => $request['namaDepan'],
            "nama_belakang" => $request['namaBelakang'],
            "jenis_kelamain" => $request['gender'],
            "nationality" => $request['nationality'],
            "language" => $request['Language'],
            "Bio" => $request['bio'],

        ];

        return view('home', compact('data'));
    }

}